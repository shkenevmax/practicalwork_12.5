#pragma once

#include "Complexity.h"
#include "Enemy.h"
#include "ComplexityLich.generated.h"

// ������ ����� ���� ��� ��������� ������ ���������

UCLASS()
class AChildLich : public AEnemy, private FComplexityChildInfo
{
	GENERATED_BODY()

public:
	virtual void Fight() override;
	virtual void UseSpell() override;
	virtual void SetupWithInfo(void* InfoPtr) override;
};

inline void AChildLich::SetupWithInfo(void* InfoPtr)
{
	auto ComplexityChild = dynamic_cast<FComplexityChildInfo>(InfoPtr);
	if (ComplexityChild)
	{
		EnemyMeleeDamage = ComplexityChild->EnemyMeleeDamage;
	}
}

UCLASS()
class ASoldierLich : public AEnemy, private FComplexitySoldierInfo
{
	GENERATED_BODY()

public:
	virtual void Fight() override;
	virtual void UseSpell() override;
	virtual void SetupWithInfo(void* InfoPtr) override;
};

inline void ASoldierLich::SetupWithInfo(void* InfoPtr)
{
	auto ComplexitySoldier = dynamic_cast<FComplexitySoldierInfo>(InfoPtr);
	if (ComplexitySoldier)
	{
		EnemyMeleeDamage = ComplexitySoldier->EnemyMeleeDamage;
		EnemySpellDamage = ComplexitySoldier->EnemySpellDamage;
	}
}

UCLASS()
class ADemonLich : public AEnemy, private FComplexityDemonInfo
{
	GENERATED_BODY()

public:
	virtual void Fight() override;
	virtual void UseSpell() override;
	virtual void SetupWithInfo(void* InfoPtr) override;
};

inline void ADemonLich::SetupWithInfo(void* InfoPtr)
{
	auto ComplexityDemon = dynamic_cast<FComplexityDemonInfo>(InfoPtr);
	if (ComplexityDemon)
	{
		EnemyMeleeDamage = ComplexityDemon->EnemyMeleeDamage;
		EnemySpellDamage = ComplexityDemon->EnemySpellDamage;
		EnemyArmor = ComplexityDemon->EnemyArmor;
	}
}