#pragma once

#include "Complexity.h"
#include "Enemy.h"
#include "ComplexitySkeleton.generated.h"

// ������ ����� ���� ������ ��������� ������ ���������

UCLASS()
class AChildSkeleton : public AEnemy, private FComplexityChildInfo
{
	GENERATED_BODY()

public:
	virtual void Fight() override;
	virtual void UseSpell() override;
	virtual void SetupWithInfo(void* InfoPtr) override;
};

inline void AChildSkeleton::SetupWithInfo(void* InfoPtr)
{
	auto ComplexityChild = dynamic_cast<FComplexityChildInfo>(InfoPtr);
	if (ComplexityChild)
	{
		EnemyMeleeDamage = ComplexityChild->EnemyMeleeDamage;
	}
}

UCLASS()
class ASoldierSkeleton : public AEnemy, private FComplexitySoldierInfo
{
	GENERATED_BODY()

public:
	virtual void Fight() override;
	virtual void UseSpell() override;
	virtual void SetupWithInfo(void* InfoPtr) override;
};

inline void ASoldierSkeleton::SetupWithInfo(void* InfoPtr)
{
	auto ComplexitySoldier = dynamic_cast<FComplexitySoldierInfo>(InfoPtr);
	if (ComplexitySoldier)
	{
		EnemyMeleeDamage = ComplexitySoldier->EnemyMeleeDamage;
		EnemySpellDamage = ComplexitySoldier->EnemySpellDamage;
	}
}

UCLASS()
class ADemonSkeleton : public AEnemy, private FComplexityDemonInfo
{
	GENERATED_BODY()

public:
	virtual void Fight() override;
	virtual void UseSpell() override;
	virtual void SetupWithInfo(void* InfoPtr) override;
};

inline void ADemonSkeleton::SetupWithInfo(void* InfoPtr)
{
	auto ComplexityDemon = dynamic_cast<FComplexityDemonInfo>(InfoPtr);
	if (ComplexityDemon)
	{
		EnemyMeleeDamage = ComplexityDemon->EnemyMeleeDamage;
		EnemySpellDamage = ComplexityDemon->EnemySpellDamage;
		EnemyArmor = ComplexityDemon->EnemyArmor;
	}
}