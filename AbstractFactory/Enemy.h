#pragma once

#include "GameFramework/Actor.h"
#include "Enemy.generated.h"

class USkeletalMesh;

// ����� �������������� �����

UCLASS()
class AEnemy : public AActor
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
		virtual void Fight() PURE_VIRTUAL(AEnemy::Fight,); // ����� � ������� ���
	UFUNCTION(BlueprintCallable)
		virtual void UseSpell() PURE_VIRTUAL(AEnemy::UseSpell,); // ����� �����������

		virtual void SetupWithInfo(void* InfoPtr) = 0;

private:

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Meshes)
		USkeletalMesh* ItemMesh { nullptr };
};