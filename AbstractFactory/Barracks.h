#pragma once

#include "GameFramework/Actor.h"
#include "ElvenLoot.h"
#include "Necroloot.h"
#include "Barracks.generated.h"

// ������� ��� ���������� ��������

class AEnemy;
UCLASS()
class ABarracks : public AActor
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintPure)
		virtual AEnemy* PrepareGoblin() PURE_VIRTUAL(ABarracks::PrepareGoblin, return nullptr;);
	UFUNCTION(BlueprintCallable, BlueprintPure)
		virtual AEnemy* PrepareSkeleton() PURE_VIRTUAL(ABarracks::PrepareSkeleton, return nullptr;);
	UFUNCTION(BlueprintCallable, BlueprintPure)
		virtual AEnemy* PrepareLich() PURE_VIRTUAL(ABarracks::PrepareLich, return nullptr;);

protected:
	void* GetInfoForItem(UClass* EnemyClass);
};

// ������ ��������� ����, ���� ������

UCLASS()
class AComplexityChild : public ABarracks
{
	GENERATED_BODY()

public:
	virtual AEnemy* PrepareGoblin() override;
	virtual AEnemy* PrepareSkeleton() override;
	virtual AEnemy* PrepareLich() override;
};

inline AEnemy* AComplexityChild::PrepareGoblin()
{
	auto Enemy = dynamic_cast<AComplexityChild*>(GetWorld()->SpawnActor(AComplexityChild::StaticClass()));
	if (Enemy)
	{
		Enemy->SetupWithInfo(GetInfoForItem(AComplexityChild::StaticClass()));
		return Enemy;
	}
}

// ������� ������� ���������

UCLASS()
class AComplexitySolger : public AForge
{
	GENERATED_BODY()

public:
	virtual AEnemy* PrepareGoblin() override;
	virtual AEnemy* PrepareSkeleton() override;
	virtual AEnemy* PrepareLich() override;
};

// ������� ������� ���������

UCLASS()
class AComplexityDemon : public AForge
{
	GENERATED_BODY()

public:
	virtual AEnemy* PrepareGoblin() override;
	virtual AEnemy* PrepareSkeleton() override;
	virtual AEnemy* PrepareLich() override;
};