#pragma once

#include ...

// �������� ������� ���������

USTRUCT(BlueprintType)
struct FComplexityChildInfo
{
	GENERATED_BODY()

public:

	// ���� ����� ��������� ������ � ������� ���
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = LootStats)
		uint64 EnemyMeleeDamage { 0 };
};

USTRUCT(BlueprintType)
struct FComplexitySoldierInfo
{
	GENERATED_BODY()

public:

	// ���� �� ������ ����� ������� ����, �� � ��������� �������� ������������
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = LootStats)
		uint64 EnemyMeleeDamage { 0 };

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = LootStats)
		uint64 EnemySpellDamage { 0 };
};

USTRUCT(BlueprintType)
struct FComplexityDemonInfo
{
	GENERATED_BODY()

public:

	// ������ ����� ������� ���� �������� ��������� �����
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = LootStats)
		uint64 EnemyMeleeDamage { 0 };

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = LootStats)
		uint64 EnemySpellDamage { 0 };

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = LootStats)
		uint64 EnemyArmor { 0 };
};