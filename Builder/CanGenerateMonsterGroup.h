#pragma once

#include "UObject/Interface.h"
#include "CanGenerateMonsterGroup.generated.h"

UINTERFACE(MinimalAPI, meta = (CannotImplementInterfaceInBlueprint))
class UCanGenerateMonsterGroup : public UInterface
{
	GENERATED_BODY()
};

class ICanGenerateMonsterGroup
{
	GENERATED_BODY()

public:
	// ���������� �������� ������� � �������� ���� ����� ��������
	UFUNCTION(BlueprintCallable, Category = "City|Generator")
		virtual UObject* GetResult() const PURE_VIRTUAL(ICanGenerateMonsterGroup::GetResult, return nullptr;);
	UFUNCTION(BlueprintCallable, Category = "City|Generator")
		virtual void GenerateSoldiers() PURE_VIRTUAL(ICanGenerateMonsterGroup::GenerateSoldiers, );
	UFUNCTION(BlueprintCallable, Category = "City|Generator")
		virtual void GenerateMage() PURE_VIRTUAL(ICanGenerateMonsterGroup::GenerateMage, );
	UFUNCTION(BlueprintCallable, Category = "City|Generator")
		virtual void GenerateBowmans() PURE_VIRTUAL(ICanGenerateMonsterGroup::GenerateBowmans, );
};