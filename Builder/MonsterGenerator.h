#pragma once

#include "UObject/UObject.h"
#include "CanGenerateMonsterGroup.h"
#include "MonsterGenerator.generated.h"

UCLASS(Blueprintable, BlueprintType)
class UGoblinsGenerator : public UObject, public ICanGenerateMonsterGroup
{
	GENERATED_BODY()

public:
	// �������������� �������� ������� ����������
	virtual UObject* GetResult const override;
	virtual void GenerateSoldiers() override;
	virtual void GenerateMage() override;
	virtual void GenerateBowmans() override;

protected:
	// ���������� ���� ���� ��������, ������� ����� ������ � �������� ������
	void SpawnLeader();
	void SpawnSwordShield();
	void SpawnFireMage();
	void SpawnNecromancer();
	void SpawnFireBowman();
	void SpawnBowman();

private:
	// ���������� �������� ������� ������
	UPROPERTY(meta = (AllowPrivateAccess = "true")), BlueprintReadOnly, VisibleAnywhere, Category = MonsterGroup)
	AActor* Leader{ nullptr };
};

// ���������� �������� �������
inline UObject* UGoblinsGenerator::GetResult() const
{
	return Leader;
}

// ���������� ������ �������� ���
inline void UGoblinsGenerator::GenerateSoldiers()
{
	SpawnLeader();
	SpawnSwordShield();
}

// ���������� �����
inline void UGoblinsGenerator::GenerateMage()
{
	SpawnFireMage();
	SpawnNecromancer();
}

// ���������� ��������
inline void UGoblinsGenerator::GenerateBowmans()
{
	void SpawnFireBowman();
	void SpawnBowman();
}

UCLASS(Blueprintable, BlueprintType)
class USkeletonGenerator : public UObject, public ICanGenerateMonsterGroup
{
	GENERATED_BODY()

public:
	virtual UObject* GetResult const override;
	virtual void GenerateSoldiers() override;
	virtual void GenerateMage() override;
	virtual void GenerateBowmans() override;

protected:
	void SpawnLeader();
	void SpawnSwordShield();
	void SpawnTwoHandedSword();
	void SpawnFireMage();
	void SpawnNecromancer();
	void SpawnFireBowman();
	void SpawnBowman();

private:
	UPROPERTY(meta = (AllowPrivateAccess = "true")), BlueprintReadOnly, VisibleAnywhere, Category = MonsterGroup)
	AActor* Leader{ nullptr };
};

inline UObject* USkeletonGenerator::GetResult() const
{
	return Leader;
}

inline void USkeletonGenerator::GenerateSoldiers()
{
	void SpawnLeader();
	void SpawnSwordShield();
	void SpawnTwoHandedSword();
}

inline void USkeletonGenerator::GenerateMage()
{
	void SpawnFireMage();
	void SpawnNecromancer();
}

inline void USkeletonGenerator::GenerateBowmans()
{
	void SpawnFireBowman();
	void SpawnBowman();
}

UCLASS(Blueprintable, BlueprintType)
class ULichGenerator : public UObject, public ICanGenerateMonsterGroup
{
	GENERATED_BODY()

public:
	virtual UObject* GetResult const override;
	virtual void GenerateSoldiers() override;
	virtual void GenerateMage() override;

protected:
	void SpawnLeader();
	void SpawnTwoHandedSword();
	void SpawnFireMage();
	void SpawnNecromancer();
	void SpawnFrostMage();

private:
	UPROPERTY(meta = (AllowPrivateAccess = "true")), BlueprintReadOnly, VisibleAnywhere, Category = MonsterGroup)
	AActor* Leader{ nullptr };
};

inline UObject* ULichGenerator::GetResult() const
{
	return Leader;
}

inline void ULichGenerator::GenerateSoldiers()
{
	void SpawnLeader();
	void SpawnTwoHandedSword();
}

inline void ULichGenerator::GenerateMage()
{
	void SpawnFireMage();
	void SpawnNecromancer();
	void SpawnFrostMage();
}