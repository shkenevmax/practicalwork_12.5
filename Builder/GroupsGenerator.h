#pragma once

#include "GameFramework/Actor.h"
#include "GenerationDirector.h"
#include "MonsterGenerator.h"
#include "GroupsGenerator.generated.h"

class UGenerationDirector;
class UGoblinsGenerator;
class USkeletonGenerator;
class ULichGenerator;

UCLASS()
class AGroupsGenerator : public AActor
{
	GENERATED_BODY()

public:
	void GenerateGroups();

private:
	UPROPERTY(meta = (AllowPrivateAccess = "true")), BlueprintReadOnly, VisibleAnywhere, Category = MonsterGroup)
		TArray<AActor*> Groups;
};

template <class TClass>
ICanGenerateMonsterGroup* CastToCanGenerateGroup(TClass* Object)
{
	// �������� ��������� �� ��������� � �������� ����� ������, ����� ����� ������ ������������� �� ����� � �������� �����
	return static_cast<ICanGenerateMonsterGroup*>(Object->GetInterfaceAdress(UCanGenerateMonsterGroup::StaticClass()));
}

inline void AGroupsGenerator::GenerateGroups()
{
	// �������� ������ �������� �� �����
	auto GoblinsGenerator = NewObject<UGoblinsGenerator>(this);
	auto IGenerator = CastToCanGenerateGroup(GoblinsGenerator);
	UGenerationDirector::GenerateGoblinGroup(IGenerator);
	Groups.Emplace(GoblinsGenerator->GetResult());

	auto SkeletonGenerator = NewObject<USkeletonGenerator>(this);
	auto IGenerator = CastToCanGenerateGroup(SkeletonGenerator);
	UGenerationDirector::GenerateSkeletonGroup(IGenerator);
	Groups.Emplace(SkeletonGenerator->GetResult());

	auto LichGenerator = NewObject<ULichGenerator>(this);
	auto IGenerator = CastToCanGenerateGroup(LichGenerator);
	UGenerationDirector::GenerateLichGroup(IGenerator);
	Groups.Emplace(LichGenerator->GetResult());
}