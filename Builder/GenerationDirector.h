#pragma once

#include "CanGenerateMonsterGroup.h"
#include "GenerationDirector.generated.h"

UCLASS()
class UGenerationDirector : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	static void GenerateGoblinGroup(ICanGenerateMonsterGroup* Generator);
	static void GenerateSkeletonGroup(ICanGenerateMonsterGroup* Generator);
	static void GenerateLichGroup(ICanGenerateMonsterGroup* Generator);
};

// ������ ������ �������� ��������� �����

inline void UGenerationDirector::GenerateGoblinGroup(ICanGenerateMonsterGroup* Generator)
{
	if (Generator)
	{
		Generator->GenerateSoldiers();
		Generator->GenerateMage();
		Generator->GenerateBowmans();
	}
}

inline void UGenerationDirector::GenerateSkeletonGroup(ICanGenerateMonsterGroup* Generator)
{
	if (Generator)
	{
		Generator->GenerateSoldiers();
		Generator->GenerateMage();
		Generator->GenerateBowmans();
	}
}

inline void UGenerationDirector::GenerateLichGroup(ICanGenerateMonsterGroup* Generator)
{
	if (Generator)
	{
		Generator->GenerateSoldiers();
		Generator->GenerateMage();
	}
}