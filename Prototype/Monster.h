#pragma once

#include "UObject/Interface.h"
#include "Monster.generated.h"

UINTERFACE(MinimalAPI, meta = (CannotImplementInterfaceInBlueprint))
class UMonster : public UInterface
{
	GENERATED_BODY()
};


// ��������� ��� ������������

class IMonster
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, category = Prototype)
		virtual void* Clone() PURE_VIRTUAL(ICanClone::Clone, return nullptr;); // ��������� � � �����������
};