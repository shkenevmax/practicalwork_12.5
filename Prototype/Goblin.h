#pragma once

#include "UObject/Object.h"
#include "Monster.h"
#include "Goblin.generated.h"

UCLASS(Blueprintable, BlueprintType)
class UGoblin : public UObject, public IMonster
{
	GENERATED_BODY()

public:
	// ��������� ������� ��������� ����������
	virtual void* Clone() override;
	float GetHealth() const
	{
		return Health;
	}
	float GetDamage() const
	{
		return Damage;
	}

private:
	// ��������� ��������� �������
	UPROPERTY(meta = (AllowPrivateAccess = "true", EditAnywhere, BlueprintReadWrite, category = Goblin)
		float Health{ 0.0f };
	UPROPERTY(meta = (AllowPrivateAccess = "true"), EditAnywhere, BlueprintReadWrite, category = Goblin)
		float Damage{ 0.0f };
};

inline void UGoblin::Clone()
{
	// �������� �����, � ��������� ����������
	auto Copy = NewObject<UGoblin>(GetOuter());
	if (IsValid(Copy))
	{
		Copy->Health = GetHealth();
		Copy->Damage = GetDamage();
	}
	return Copy;
}