#pragma once

#include "Goblin.h"
#include "Spawner.generated.h"

UCLASS()
class USpawner : public UObject
{
	GENERATED_BODY()

public:
	static USpawner* Get(); // ������� ��� �������� ���������
	void* Clone(IMonster* From);

private:
	static USpawner* Instance;
};

inline USpawner* USpawner::Get()
{
	// ��������� ���� �� ��������� ������, ���� ���, �� ������, ���� ���, �� ���������� �������� ������ ����������
	if (!Instance)
	{
		Instance = NewObject<USpawner>();
	}
	return Instance;
}

inline void* USpawner::Clone(IMonster* From)
{
	// ��������� ������������ ������� � ���������� ���������
	return From->Clone();
}